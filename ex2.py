# using python 3.6
# reviewing code is fun, isn't it?
# especially when it's this simple.

def isWhiteLine(mystring):
    # "strip" empties the string if it only contains whitespace and tab
    # and empty string are falsy in nature
    if mystring.strip():
        return False        # returns false if string contains something other than whitespace tab etc
    else:
        return True         # functions returns true if empty as you asked
        

while True:
    print("If you're running this in cmd then you can just drag the file on me... or else")
    # because windows will add double quotes if filename of dragged file contains spaces
    filename = str(input("Enter the File path please (ascii txt file): "))
    # so we need to get rid of those double quotes
    filename = filename.replace("\"", "")

    try:
        myfile = open(filename,'r')
    except OSError:
        print("Error Opening File")
        continue
    while True:
        mystring = myfile.readline()
        if not isWhiteLine(mystring):
            print (mystring, end="")
