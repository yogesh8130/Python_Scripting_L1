# using python 3.6


def isListOfInts(anyList):
    print("entered list is", anyList)
    if (isinstance(anyList, list)):
        return(all(isinstance(x, int) for x in anyList))
    else:
        raise ValueError("Parameter", anyList, "is not a list.")


print(bool(isListOfInts([])))
print(bool(isListOfInts([1])))
print(bool(isListOfInts([1,2])))
print(bool(isListOfInts([0])))
print(bool(isListOfInts(['1'])))
print(bool(isListOfInts([1,'a'])))
print(bool(isListOfInts(['a',1])))
print(bool(isListOfInts([1, 1.])))
print(bool(isListOfInts([1., 1.])))
print(bool(isListOfInts((1,2))))
