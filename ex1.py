# Python version – 3.6
# hey there!! hope you're having a good day.


def ruler(num):
    # Using a static label for sub-scale allows us to easily change the label without coding much
    # These numbers only go upto 10 so hardcoding the subscale "rulerlabels" is rather efficient.
    rulerlabels = "1234567890"
    subscale = num%10        # getting the units digit
    mainscale = num//10     # seperating all digits from units digit by using floor division

    topnumber = 1           # initializing
    print("")

    # Printing the mainscale (the top scale)
    count = mainscale
    if (mainscale < 1):
        print("")
    else:
        while (count > 0):
            numberOfDigits = len(str(topnumber))
            # now all we have to do is get the number of spaces to add between consecutive numbers
            spacecount = 10 - numberOfDigits
            print (" " * spacecount, end="")
            print (topnumber, end="")
            count -= 1
            topnumber += 1
        print("")

    # this block is only for aethetic purposes... makes output look like a real scale
    count = mainscale
    while (count > 0):
        print ("┬" * 9, end="")
        print ("┼", end="")
        count -= 1
    print("┬" * subscale, end="")
    print("")
    
    # print the subscale (the bottom scale)
    print(rulerlabels * mainscale, end="")          # just print the labels "mainscale" number of times
    print(rulerlabels[:subscale])                   # add few extra labels at the end that still need to be printed

    # just adding some padding before Printing next scale
    # hey that rhymed
    print("")
    print("")

while(True):
    try:
        num = int(input("Enter an integer: "))
    except ValueError:
        print("")
        print("wait a minute!!!")
        print("That's not an int!")
        print("")
        continue
    ruler(num)
